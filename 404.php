<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Olympus
 */

get_header();
?>

<section>
	<div class="container mx-auto p-sm py-lg text-center">
		<div class="d-inline-block text-center">
			<h1 class="c-orange f-mulish fs-lg fw-800 mb-md"><?php echo esc_html__( 'Oops!', 'olympus' ); ?></h1>
			<p class="ln-1 f-mulish fs-md-1 fw-600 mb-md"><?php echo esc_html__( '404 page not found', 'olympus' ); ?></p>
			<div class="mb-md">
			<form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input type="text" class="p-xm outline-none input-fs f-mulish c-offblack border-1 c-black br-sm" name="s" placeholder="Search..." value="<?php echo get_search_query(); ?>">
				<button class="input-fs bg-primary border-none cr-pointer subscribe-btn-sm border-1 border-white tx-negative-xm c-white f-mulish" type="submit">
					<span class="iconify h3" data-icon="fluent:search-16-filled"></span>
				</button> 
			</form>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();