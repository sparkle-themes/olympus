<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Olympus
 */

get_header();
?>

<section>
	<div class="hero bg-default">
		<div class="bg-primary-1">
			<div class="container c-white mx-auto py-7 p-sm text-center">
				<h1 class="h2 f-mulish mb-md"><?php echo esc_html( single_cat_title() ); ?></h1>
				<p class="f-mulish fs-md-1">Home > <span class="c-orange"><?php echo esc_html( single_cat_title() ); ?></span> </p>
			</div>
		</div>
	</div>
</section>

		<?php if ( have_posts() ) : ?>

			<section>
				<div class="container mx-auto p-sm py-lg">
					<div class="d-flex">
						<div class="flex-grow flex-shrink">
							<div class="d-flex align-center justify-between mb-lg">

								<div class="border-1 d-inline-block p-xm px-sm br-3">
									<button class="btn-d-none c-orange-states cr-pointer c-offblack bg-white border-none">
										<span class="iconify  icon-2" data-icon="ant-design:appstore-filled"></span>
									</button>

									<button class="btn-d-none c-orange-states cr-pointer c-offblack bg-white border-none">
										<span class="iconify icon-2" data-icon="ic:round-apps"></span>
									</button>

									<button class="btn-d-none c-orange-states cr-pointer c-offblack bg-white border-none">
										<span class="iconify icon-2 c-orange" data-icon="dashicons:menu-alt"></span>
									</button>
								</div>

								<div class="border-1 d-inline-block p-sm px-sm br-3">
									<select class="bg-white cr-pointer border-none c-offblack btn-d-none fw-600" name="" id="">
										<option selected value="">Sort by: Name</option>
										<option value="">Sort by: Date</option>
									</select>
								</div>
							</div>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			olympus_numbered_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

				</div>
			<?php get_sidebar(); ?>
		</div>
	</section>

<?php
get_footer();