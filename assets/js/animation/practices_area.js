const opts = {
    threshold: [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
    rootMargin: "0px 0px -20px 0px"
}

magicScroller($(".fade__animate"), opts, "animate__fadeInUp");
magicScroller($(".fadein__animate"), opts, "animate__fadeIn");
magicScroller($(".fadeinleft__animate"), opts, "animate__fadeInLeft");
magicScroller($(".fadeinright__animate"), opts, "animate__fadeInRight");
magicScroller($(".zoomindown__animate"), opts, "animate__zoomInDown");
magicScroller($(".bounce__animate"), opts, "animate__bounce");
magicScroller($(".backinleft__animate"), opts, "animate__bounceInLeft");
magicScroller($(".flipiny__animate"), opts, "animate__flipInY");
magicScroller($(".zoominright__animate"), opts, "animate__zoomInRight");
magicScroller($(".lightspeedinleft__animate"), opts, "animate__lightSpeedInLeft");
magicScroller($(".lightspeedinright__animate"), opts, "animate__lightSpeedInRight");
magicScroller($(".slideinright__animate"), opts, "animate__slideInRight");