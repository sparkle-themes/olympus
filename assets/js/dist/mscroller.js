/* 
description: Magic Scroller created by Sagar Magar
Author: "Sagar Magar"

*/

function magicScroller(elem, obj, cname) {
    let target;

    let observer;

    if (screen.width >= 992) {
        target = elem;
        target.addClass("animate__animated")
        target.addClass("v-hidden");

        observer = new IntersectionObserver((entries, observer) => {
            const entry = entries[0];

            if (entry.isIntersecting) {
                if (!$(entry.target).hasClass(cname)) {
                    $(entry.target).toggleClass("v-hidden");
                    $(entry.target).addClass(cname)
                }
            }
        }, obj)

        target.each((ind, elem) => {
            observer.observe(elem)
        })
    }

}