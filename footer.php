<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Olympus
 */

?>

<footer>
    <div class="bg-primary">
        <div class="container d-flex justify-between flex-wrap p-sm py-lg mx-auto c-white">
            <div class="max-w-23 mr-1 mb-md">
                <?php dynamic_sidebar( 'footer-1' ); ?>
            </div>

            <div class="mr-1 mb-md">
                <?php dynamic_sidebar( 'footer-2' ); ?>
            </div>

            <div class="mr-1 mb-md">
                <?php dynamic_sidebar( 'footer-3' ); ?>
            </div>


            <div class="mr-1 mb-md">
                <?php dynamic_sidebar( 'footer-4' ); ?>
            </div>

            <div class="mb-md">
                <?php dynamic_sidebar( 'footer-5' ); ?>
            </div>

            <p class="c-offwhite ln-1 flex-shrink-0 flex-b-100 text-center">
                <?php apply_filters( 'construction_light_copyright', 5 ); ?> <?php the_privacy_policy_link(); ?>
            </p>

        </div>
    </div>
</footer>


<?php wp_footer(); ?>

</body>
</html>