<?php 
    get_header();

    $olympus_homepage_sections = olympus_homepage_section();

    foreach ($olympus_homepage_sections as $olympus_homepage_section) {

        $olympus_homepage_section = str_replace('olympus_', '', $olympus_homepage_section);
        $olympus_homepage_section = str_replace('_section', '', $olympus_homepage_section);

        get_template_part( 'section/section', $olympus_homepage_section );
    }

    get_footer();
?>