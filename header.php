<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Olympus
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<div id="modal__search" class="d-none p-sm box-content">
	<div class="d-flex justify-center">
		<form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="text" class="p-xm outline-none input-fs f-mulish c-offblack border-1 c-black br-sm" name="s" placeholder="Search..." value="<?php echo get_search_query(); ?>">
			<button class="input-fs bg-primary border-none cr-pointer subscribe-btn-sm border-1 border-white tx-negative-xm c-white f-mulish" type="submit">
				<span class="iconify h3" data-icon="fluent:search-16-filled"></span>
			</button> 
		</form>
	</div>
</div>

<?php if( get_theme_mod( 'olympus_top_header_enable_disable', 'enable' ) === 'enable' ) : ?>
<aside>
	<div class="bg-primary">
		<div class="container mx-auto">
			<div class="color-primary d-flex justify-between flex-wrap p-sm align-center">
				<div class="d-flex mb-sm--mob flex-wrap flex-b-100--mob">
					<div class="d-flex justify-center--mob align-center mr-1 flex-b-100--mob mb-sm--mob">
						<i class="iconify c-orange <?php echo esc_attr( get_theme_mod( 'olympus_email_icon' ) ); ?>"></i>
						<p class="ml-sm">
							<a class="c-white p-link" href="<?php echo esc_url( 'mailto:' . get_theme_mod('olympus_email_id') ); ?>"><?php echo esc_html( get_theme_mod('olympus_email_id') ); ?></a>
						</p>
					</div>

					<div class="d-flex justify-center--mob align-center flex-b-100--mob">
						<i class="iconify c-orange <?php echo esc_attr( get_theme_mod( 'olympus_phone_icon' ) ); ?>"></i>
						<p class="ml-sm">
							<a class="c-white p-link" href="<?php echo esc_url( 'tel:' . get_theme_mod('olympus_phone_no') ); ?>"><?php echo esc_html( get_theme_mod( 'olympus_phone_no' ) ); ?></a>
						</p>
					</div>
				</div>

				<div class="flex-b-100--mob d-flex justify-center--mob">
					<?php $olympus_top_header_social = json_decode( get_theme_mod( 'olympus_top_header_social' ) ); ?>
					<?php 
						foreach ( $olympus_top_header_social as $olympus_top_header_social ) { ?>
							<a href="<?php echo esc_url( $olympus_top_header_social->social_link ); ?>">
								<i class="iconify iconify--grommet-icons c-white mr-1 <?php echo esc_attr( $olympus_top_header_social->icon ) ?>"></i>
							</a>
					<?php }	?>
				</div>
			</div>
		</div>
	</div>
</aside>
<?php endif; ?>

<header>
	<div class="container mx-auto">
		<div class="d-flex align-center p-sm flex-wrap justify-between">
			<button id="ham-menu" class="mr-2 cr-pointer h2 d-none--lp-u flex-shrink-0 box-content p-txm btn bg-white d-flex align-center">
				<span class="iconify c-black" data-icon="majesticons:menu-line"></span>
			</button>

			<?php 
                if( function_exists( 'the_custom_logo' ) ) {
                    
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                    $logo           = wp_get_attachment_image_src( $custom_logo_id );

                }
            ?>

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img class="mr-2 flex-shrink-0" src="<?php echo esc_url( $logo[0] ); ?>" alt="">
			</a>

			<nav class="d-none--lp">

			<?php

				wp_nav_menu(
					array(
						'menu' => 'primary',
						'menu_class' => 'ls-none d-flex flex-wrap align-center',
						'container' => '',
						'theme_location' => 'primary',
					)
				)

			?>	

			</nav>

			<button id="search-trigger" class="btn-search outline-none ">
				<span class="iconify trigger-search cr-pointer h3 p-txm br-50 border-1 border-black box-content"
					data-icon="fluent:search-16-filled"></span>
				<span
					class="iconify trigger-cross d-none cr-pointer h3 p-txm br-50 border-1 border-black box-content"
					data-icon="gridicons:cross-small"></span>
			</button>
		</div>
	</div>
</header>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>