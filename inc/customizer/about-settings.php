<?php

$wp_customize->add_panel( 'olympus_about_us_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'About Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_about_section', array(
    'title' => esc_html__('About', 'olympus'),
    'panel' => 'olympus_about_us_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_about_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_about_enable_disable', array(
    'section' => 'olympus_about_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_about_us_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_about_us_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_about_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_about_us_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_about_us_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_about_section',
    'type' => 'textarea',
));

$wp_customize->add_setting( 'about_us_featured_image', array(
    'transport' => 'postMessage',
    'sanitize_callback'	=> 'esc_url_raw'		//done
));

$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'about_us_featured_image', array(
    'label'	   => esc_html__( 'Featured Image', 'olympus' ),
    'section'  => 'olympus_about_section',
    'type'	   => 'image',
)));

$wp_customize->add_setting( 'olympus_about_us_features', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control( new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_about_us_features', 
    array(
        'label' 	   => esc_html__('Features Items', 'olympus'),
        'section' 	   => 'olympus_about_section',
        'settings' 	   => 'olympus_about_us_features',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'feature_icon' 	=> array(
            'type'    => 'icons',
            'label'   => esc_html__('Icon', 'olympus'),
            'default' => ''
        ),

        'feature_title' => array(
            'type' => 'text',
            'label' => esc_html__('Title', 'olympus'),
            'default' => ''
        ),

        'feature_desc' => array(
            'type' => 'textarea',
            'label' => esc_html__('Description', 'olympus'),
            'default' => ''
        ),        
    )
));


?>