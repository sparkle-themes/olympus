<?php

$wp_customize->add_panel( 'olympus_blog_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Blog Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_blog_section', array(
    'title' => esc_html__( 'Blog', 'olympus' ),
    'panel' => 'olympus_blog_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_blog_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_blog_enable_disable', array(
    'section' => 'olympus_blog_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_blog_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_blog_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_blog_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_blog_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_blog_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_blog_section',
    'type' => 'textarea',
));

$wp_customize->add_setting('olympus_blog_cat', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',     //done
));

$wp_customize->add_control(new Construction_Light_Multiple_Check_Control($wp_customize, 'olympus_blog_cat', array(
    'label'    => esc_html__('Select Category To Show Posts', 'olympus'),
    'settings' => 'olympus_blog_cat',
    'section'  => 'olympus_blog_section',
    'choices'  => $blog_cat,
)));

$wp_customize->add_setting( 'olympus_blog_posts_no',array(
    'default'			 =>	'3',
    'transport'          => 'postMessage',
    'sanitize_callback'	 =>	'olympus_sanitize_select'		//done	
));

$wp_customize->add_control( 'olympus_blog_posts_no', array(
    'label'	  =>	esc_html__('Number of Posts to display','olympus'),
    'section' =>	'olympus_blog_section',
    'type'	  =>	'select',
    'choices' => array(
        '2'       => esc_html__( '2 Posts', 'olympus' ),
        '3'     => esc_html__( '3 Posts', 'olympus' ),
        '4'      => esc_html__( '4 Posts', 'olympus' ),
        '5'      => esc_html__( '5 Posts', 'olympus' ),
        '6'       => esc_html__( '6 Posts', 'olympus' ),
    )
));

$wp_customize->add_setting( 'olympus_blog_col', array(
    'sanitize_callback' => 'absint' 
));
  
$wp_customize->add_control( 'olympus_blog_col', array(
    'label' => esc_html__( 'Number of Columns', 'olympus' ),
    'section' => 'olympus_blog_section',
    'type' => 'number',
    'input_attrs' => array(
        'min' => '1', 'step' => '1', 'max' => '4',
    ),
)); 

$wp_customize->add_setting( 'olympus_blog_button_text', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_blog_button_text', array(
    'label' => esc_html__( 'Button Text', 'olympus' ),
    'type'  => 'text',
    'section' => 'olympus_blog_section',
));

?>