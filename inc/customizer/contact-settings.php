<?php

$wp_customize->add_panel( 'olympus_contact_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Contact Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_contact_section', array(
    'title' => esc_html__( 'Contact', 'olympus' ),
    'panel' => 'olympus_contact_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_contact_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_contact_enable_disable', array(
    'section' => 'olympus_contact_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_contact_page', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_contact_page', array(
    'label' => 'Select Page',
    'type'  => 'dropdown-pages',
    'section' => 'olympus_contact_section',
));

$wp_customize->add_setting( 'olympus_contact_short_code', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_contact_short_code', array(
    'label' => esc_html__( 'Short Code', 'olympus' ),
    'type'  => 'text',
    'section' => 'olympus_contact_section',
));

$wp_customize->add_setting( 'olympus_contact_name', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_contact_name', array(
    'label' => esc_html__( 'Name', 'olympus' ),
    'type'  => 'text',
    'section' => 'olympus_contact_section',
));

$wp_customize->add_setting( 'olympus_contact_designation', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_contact_designation', array(
    'label' => esc_html__( 'Designation', 'olympus' ),
    'type'  => 'text',
    'section' => 'olympus_contact_section',
));

$wp_customize->add_setting( 'olympus_contact_signature', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

$wp_customize->add_control( 'olympus_contact_signature', array(
    'label' => esc_html__( 'Signature', 'olympus' ),
    'type'  => 'text',
    'section' => 'olympus_contact_section',
));

?>