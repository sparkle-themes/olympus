<?php 

$wp_customize->add_panel( 'olympus_cta_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Call to Action Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_cta_section', array(
    'title' => esc_html__( 'Call to Action', 'olympus' ),
    'panel' => 'olympus_cta_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_cta_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_cta_enable_disable', array(
    'section' => 'olympus_cta_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_cta_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_cta_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_cta_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_cta_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_cta_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_cta_section',
    'type' => 'textarea',
));

$wp_customize->add_setting( 'olympus_cta_short_code', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_cta_short_code', array(
    'label' => esc_html__( 'Short Code', 'olympus' ),
    'section' => 'olympus_cta_section',
    'type' => 'text',
));

?>