<?php
/**
 * Olympus Theme Customizer
 *
 * @package Olympus
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function olympus_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'olympus_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'olympus_customize_partial_blogdescription',
			)
		);
	}

	// List All Pages
	$pages = [];

	$pages_obj = get_pages();

	$pages[''] = esc_html__('Select Page', 'olympus');

	foreach ($pages_obj as $page) {
	    $pages[$page->ID] = $page->post_title;
	}

	// List All Category
	$categories = get_categories();
	$blog_cat = array();

	foreach ($categories as $category) {
	    $blog_cat[$category->term_id] = $category->name;
	}

	require get_template_directory() . '/inc/customizer/top-header-settings.php';
	require get_template_directory() . '/inc/customizer/slider-settings.php';
	require get_template_directory() . '/inc/customizer/services-settings.php';
	require get_template_directory() . '/inc/customizer/about-settings.php';
	require get_template_directory() . '/inc/customizer/counter-settings.php';
	require get_template_directory() . '/inc/customizer/legal-practice-settings.php';
	require get_template_directory() . '/inc/customizer/notice-settings.php';
	require get_template_directory() . '/inc/customizer/contact-settings.php';
	require get_template_directory() . '/inc/customizer/tab-settings.php';
	require get_template_directory() . '/inc/customizer/testimonials-settings.php';
	require get_template_directory() . '/inc/customizer/team-settings.php';
	require get_template_directory() . '/inc/customizer/logo-settings.php';
	require get_template_directory() . '/inc/customizer/blog-settings.php';
	require get_template_directory() . '/inc/customizer/cta-settings.php';
}
add_action( 'customize_register', 'olympus_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function olympus_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function olympus_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function olympus_customize_preview_js() {
	wp_enqueue_script( 'olympus-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'olympus_customize_preview_js' );

function olympus_customize_scripts(){

	wp_enqueue_style( 'fontawesome', get_template_directory_uri(). '/assets/library/fontawesome/css/all.min.css');
 
	wp_enqueue_style('olympus-customizer', get_template_directory_uri() . '/assets/css/customizer.css');
 
	wp_enqueue_script('olympus-customizer', get_template_directory_uri() . '/assets/js/customizer-admin.js', array('jquery', 'customize-controls'), true);
	
 }
 add_action('customize_controls_enqueue_scripts', 'olympus_customize_scripts');

 if( !function_exists('olympus_homepage_section') ){

	function olympus_homepage_section(){

		$sections = [
						'olympus_top_header_settings',
						'olympus_slider_section',
						'olympus_services_section',
						'olympus_about_section',
						'olympus_counter_section',
						'olympus_legal_practice_section',
						'olympus_notice_section',
						'olympus_contact_section',
						'olympus_tab_section',
						'olympus_testimonials_section',
						'olympus_team_section',
						'olympus_logo_section',
						'olympus_blog_section',
						'olympus_cta_section'
					];
		
        return $sections;
	}
}