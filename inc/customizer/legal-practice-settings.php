<?php

$wp_customize->add_panel( 'olympus_legal_practice_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Legal Practice Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_legal_practice_section', array(
    'title' => esc_html__( 'Legal Practice', 'olympus' ),
    'panel' => 'olympus_legal_practice_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_legal_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_legal_enable_disable', array(
    'section' => 'olympus_legal_practice_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_legal_practice_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_legal_practice_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_legal_practice_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_legal_practice_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_legal_practice_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_legal_practice_section',
    'type' => 'textarea',
));

$wp_customize->add_setting( 'olympus_legal_practice_items', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control( new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_legal_practice_items', 
    array(
        'label' 	   => esc_html__( 'Legal Practice Items', 'olympus' ),
        'section' 	   => 'olympus_legal_practice_section',
        'settings' 	   => 'olympus_legal_practice_items',
        'cl_box_label' => esc_html__( 'Item #', 'olympus' ),
        'cl_box_add_control' => esc_html__( 'Add New', 'olympus' ),
    ),
    array(
        'legal_practice_icon' 	=> array(
            'type'    => 'icons',
            'label'   => esc_html__( 'Icon', 'olympus' ),
            'default' => ''
        ),

        'legal_practice_item_page' => array(
            'type' => 'select',
            'label' => esc_html__('Select Page', 'olympus'),
            'options' => $pages
        ),
    )
));

$wp_customize->add_setting( 'olympus_legal_practice_button_text', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_legal_practice_button_text', array(
    'label' => esc_html__( 'Button Text', 'olympus' ),
    'section' => 'olympus_legal_practice_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_legal_practice_button_link', array(
    'sanitize_callback' => 'esc_url_raw', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_legal_practice_button_link', array(
    'label' => esc_html__( 'Button Link', 'olympus' ),
    'section' => 'olympus_legal_practice_section',
    'type' => 'url',
));

?>