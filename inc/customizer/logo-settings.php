<?php

$wp_customize->add_panel( 'olympus_logo_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Logo Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_logo_section', array(
    'title' => esc_html__( 'Logo', 'olympus' ),
    'panel' => 'olympus_logo_panel',
    'priority' => 1,
));

$wp_customize->add_setting( 'olympus_logo_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_logo_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_logo_section',
    'type' => 'text',
));

$wp_customize->add_setting('olympus_logo_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_logo_enable_disable', array(
    'section' => 'olympus_logo_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_logo_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_logo_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_logo_section',
    'type' => 'textarea',
));

$wp_customize->add_setting( 'olympus_logo_items', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control( new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_logo_items', 
    array(
        'label' 	   => esc_html__('Logo Items', 'olympus'),
        'section' 	   => 'olympus_logo_section',
        'settings' 	   => 'olympus_logo_items',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'logo' => array(
            'type' => 'upload',
            'label' => esc_html__('Select Logo', 'olympus'),
            'default' => ''
        ),    
    )
));

?>