<?php

$wp_customize->add_panel( 'olympus_notice_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Notice Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_notice_section', array(
    'title' => esc_html__( 'Notice', 'olympus' ),
    'panel' => 'olympus_notice_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_notice_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_notice_enable_disable', array(
    'section' => 'olympus_notice_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_notice_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_notice_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_notice_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_notice_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_notice_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_notice_section',
    'type' => 'textarea',
));

//add setting
$wp_customize->add_setting( 'olympus_notice_page', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
));

//add control
$wp_customize->add_control( 'olympus_notice_page', array(
    'label' => esc_html__( 'Select Page', 'olympus' ),
    'type'  => 'dropdown-pages',
    'section' => 'olympus_notice_section',
));

?>