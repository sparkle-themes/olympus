<?php

$wp_customize->add_panel('olympus_services_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Services Settings', 'olympus' ),
));

$wp_customize->add_section('olympus_services_section', array(
    'title' => esc_html__('Services', 'olympus'),
    'panel' => 'olympus_services_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_services_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_services_enable_disable', array(
    'section' => 'olympus_services_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting('olympus_services', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control(new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_services', 
    array(
        'label' 	   => esc_html__('Service Items', 'olympus'),
        'section' 	   => 'olympus_services_section',
        'settings' 	   => 'olympus_services',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'service_item_title' => array(
            'type' => 'text',
            'label' => esc_html__('Title', 'olympus'),
            'default' => ''
        ),

        'service_item_desc' => array(
            'type' => 'textarea',
            'label' => esc_html__('Description', 'olympus'),
            'default' => ''
        ),

        'service_item_icon' 	=> array(
            'type'    => 'icons',
            'label'   => esc_html__('Icon', 'olympus'),
            'default' => ''
        ),
    )
));



?>