<?php

$wp_customize->add_panel('olympus_slider_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Slider Settings', 'olympus' ),
));

$wp_customize->add_section('olympus_slider_section', array(
    'title' => esc_html__('Slider', 'olympus'),
    'panel' => 'olympus_slider_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_slider_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => '  enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_slider_enable_disable', array(
    'section' => 'olympus_slider_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting('olympus_sliders', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control(new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_sliders', 
    array(
        'label' 	   => esc_html__('Slider Items', 'olympus'),
        'section' 	   => 'olympus_slider_section',
        'settings' 	   => 'olympus_sliders',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'slider_item_title' => array(
            'type' => 'text',
            'label' => esc_html__('Title', 'olympus'),
            'default' => ''
        ),

        'slider_item_page' => array(
            'type' => 'select',
            'label' => esc_html__('Select Slider Page', 'olympus'),
            'options' => $pages
        ),

        'slider_item_button_text' => array(
            'type' => 'text',
            'label' => esc_html__('Button Text', 'olympus'),
            'default' => ''
        ),
        
        'slider_item_button_url' => array(
            'type' => 'url',
            'label' => esc_html__('Button Url', 'olympus'),
            'default' => ''
        ),

        'slider_video_link' => array(
            'type' => 'url',
            'label' => esc_html__('Video Link', 'olympus'),
            'default' => ''
        ),
    )
));




?>