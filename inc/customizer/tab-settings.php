<?php

$wp_customize->add_panel( 'olympus_tab_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Tab Settings', 'olympus' ),
));

$wp_customize->add_section( 'olympus_tab_section', array(
    'title' => esc_html__( 'Tab', 'olympus' ),
    'panel' => 'olympus_tab_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_tab_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_tab_enable_disable', array(
    'section' => 'olympus_tab_section',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_tab_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_tab_title', array(
    'label' => esc_html__( 'Title', 'olympus' ),
    'section' => 'olympus_tab_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_tab_desc', array(
    'sanitize_callback' => 'sanitize_textarea_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_tab_desc', array(
    'label' => esc_html__( 'Description', 'olympus' ),
    'section' => 'olympus_tab_section',
    'type' => 'textarea',
));

$wp_customize->add_setting( 'olympus_tab_items', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control( new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_tab_items', 
    array(
        'label' 	   => esc_html__('Tab Items', 'olympus'),
        'section' 	   => 'olympus_tab_section',
        'settings' 	   => 'olympus_tab_items',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'tab_item_icon' 	=> array(
            'type'    => 'icons',
            'label'   => esc_html__('Icon', 'olympus'),
            'default' => ''
        ),

        'tab_item_page' => array(
            'type' => 'select',
            'label' => esc_html__('Select Page', 'olympus'),
            'options' => $pages
        ),    
    )
));

$wp_customize->add_setting( 'olympus_tab_number', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_tab_number', array(
    'label' => esc_html__( 'Number', 'olympus' ),
    'section' => 'olympus_tab_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_tab_number_title', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_tab_number_title', array(
    'label' => esc_html__( 'Number Title', 'olympus' ),
    'section' => 'olympus_tab_section',
    'type' => 'text',
));

$wp_customize->add_setting( 'olympus_tab_number_suffix', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control( 'olympus_tab_number_suffix', array(
    'label' => esc_html__( 'Suffix', 'olympus' ),
    'section' => 'olympus_tab_section',
    'type' => 'text',
));

?>