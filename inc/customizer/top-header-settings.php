<?php

$wp_customize->add_panel('olympus_header_panel', array(
    'priority'       => 3,
    'title'          => esc_html__( 'Header Settings', 'olympus' ),
));

$wp_customize->add_section('olympus_top_header_settings', array(
    'title' => esc_html__('Top Header', 'olympus'),
    'panel' => 'olympus_header_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_top_header_enable_disable', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage',
    'default' => 'enable'
));

$wp_customize->add_control(new Construction_Light_Switch_Control($wp_customize, 'olympus_top_header_enable_disable', array(
    'section' => 'olympus_top_header_settings',
    'label' => esc_html__('Enable Section ', 'olympus'),
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'olympus'),
        'disable' => esc_html__('No', 'olympus'),
    ),
    'class' => 'switch-section',
    'priority' => -1
)));

$wp_customize->add_setting( 'olympus_email_id', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control('olympus_email_id', array(
    'label'		=> esc_html__( 'Email', 'olympus' ),
    'section'	=> 'olympus_top_header_settings',
    'type'      => 'text'
));

$wp_customize->add_setting( 'olympus_email_icon', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control('olympus_email_icon', array(
    'label'		=> esc_html__( 'Email Icon', 'olympus' ),
    'section'	=> 'olympus_top_header_settings',
    'type'      => 'text'
));

$wp_customize->add_setting( 'olympus_phone_no', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control('olympus_phone_no', array(
    'label'		=> esc_html__( 'Phone Number', 'olympus' ),
    'section'	=> 'olympus_top_header_settings',
    'type'      => 'text'
));

$wp_customize->add_setting( 'olympus_phone_icon', array(
    'sanitize_callback' => 'sanitize_text_field', 	 //done	
    'transport' => 'postMessage'
));

$wp_customize->add_control('olympus_phone_icon', array(
    'label'		=> esc_html__( 'Phone Icon', 'olympus' ),
    'section'	=> 'olympus_top_header_settings',
    'type'      => 'text'
));

$wp_customize->add_section('olympus_top_header_social', array(
    'title' => esc_html__('Social', 'olympus'),
    'panel' => 'olympus_header_panel',
    'priority' => 1,
));

$wp_customize->add_setting('olympus_top_header_social', array(
    'sanitize_callback' => 'olympus_sanitize_repeater',		//done
    'transport' => 'postMessage',
    'default' => json_encode(array(
        array(
            'page'   => '',
            'price' =>'',
            'popular' =>'',
            'icon' => '',
            'type' => ''
            
        )
    ))
));

$wp_customize->add_control(new Construction_Light_Repeater_Control( $wp_customize, 
    'olympus_top_header_social', 
    array(
        'label' 	   => esc_html__('Social Items', 'olympus'),
        'section' 	   => 'olympus_top_header_social',
        'settings' 	   => 'olympus_top_header_social',
        'cl_box_label' => esc_html__('Item #', 'olympus'),
        'cl_box_add_control' => esc_html__('Add New', 'olympus'),
    ),
    array(
        'icon' 	=> array(
            'type'    => 'icons',
            'label'   => esc_html__('Icon', 'olympus'),
            'default' => ''
        ),

        'social_link' => array(
            'type' => 'url',
            'label' => esc_html__('Link', 'construction-light'),
            'default' => ''
        )
    )
));