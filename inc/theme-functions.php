<?php

if ( ! function_exists( 'construction_light_footer_copyright' ) ) {

    /**
     * Footer Copyright Information
     *
     * @since 1.0.0
     */
    function construction_light_footer_copyright() {

        echo esc_html( apply_filters( 'construction_light_copyright_text', $content = esc_html__('Copyright  &copy; ','olympus') . date( 'Y' ) . ' ' . get_bloginfo( 'name' ) .' - ' ) );

        printf( ' WordPress Theme : by %1$s', '<a href=" ' . esc_url('https://sparklewpthemes.com/') . ' " rel="designer" target="_blank">'.esc_html__('Sparkle Themes','olympus').'</a>' );
    }

}

add_action( 'construction_light_copyright', 'construction_light_footer_copyright', 5 );

if ( !function_exists( 'olympus_action_slider' ) ) {

    function olympus_action_slider() { 
            if( 'enable' != get_theme_mod( 'olympus_slider_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div>
                <div class="glide">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">

                            <?php 
                                $sliders        = json_decode( get_theme_mod( 'olympus_sliders' ) );
                                foreach( $sliders as $slider ) { 
                                        $slider_image   = get_the_post_thumbnail_url( $slider->slider_item_page );
                                    ?>
                                    <li class="glide__slide">
                                        <div class="bg-default hero__slide-1" style="background-image:url(<?php echo esc_url( $slider_image ); ?>)">
                                            <div class="bg-primary-1 w-100 h-100 py-8 box-content p-sm">
                                                <div class="container mx-auto">
                                                    <div class="max-w-600">
                                                        <p class="c-offwhite fs-md-1 ln-1 mb-md f-mulish"><?php echo esc_html__( $slider->slider_item_title ); ?></p>
                                                        <h1 class="h1 f-mulish c-white mb-md"><?php echo esc_html( get_the_title( $slider->slider_item_page ) ); ?></h1>
                                                        <p class="ln-1 c-offwhite mb-md">
                                                            <?php 
                                                                echo wp_strip_all_tags( apply_filters( 'the_excerpt', get_post_field( 'post_excerpt', $slider->slider_item_page ) ) );
                                                            ?>
                                                        </p>

                                                        <div class="d-inline-flex align-center">
                                                            <button class="bg-secondary btn-states cr-pointer br-sm ml-auto br-none c-white btn-sm mr-3">
                                                                <?php echo esc_html__( $slider->slider_item_button_text ); ?>
                                                            </button>
                                                            <div class="ps-relative">
                                                                <span
                                                                    class="iconify zi-105 btn-iframe-trigger cr-pointer ps-relative fs-md-1 box-content p-txm br-50 c-orange bg-white"
                                                                    data-icon="bi:play-fill">
                                                                </span>
                                                                <div
                                                                    class="btn-play__shadow a-blink z-100 ps-absolute br-50 bg-white t-0 r-0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                            <?php } ?>
                        
                        </ul>
                    </div>

                    <div class="glide__bullets glide__bullets-c" data-glide-el="controls[nav]">
                        <button class="glide__bullet glide__bullet-c" data-glide-dir="=0"></button>
                        <button class="glide__bullet glide__bullet-c" data-glide-dir="=1"></button>
                        <button class="glide__bullet glide__bullet-c" data-glide-dir="=2"></button>
                    </div>

                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_slider', 'olympus_action_slider', 10 );

if( !function_exists( 'olympus_action_services' ) ) {

    function olympus_action_services() { 
            if( 'enable' != get_theme_mod( 'olympus_services_enable_disable', 'enable' ) ) return;
        ?>
        <section class="ps-relative t-negative-4">
            <div class="container mx-auto p-sm text-center">
                <div class="d-flex justify-between flex-wrap">

                    <?php 
                        $services = json_decode( get_theme_mod( 'olympus_services' ) ); 
                        foreach( $services as $service ) { ?>
                            <div class="b-shadow-primary bg-secondary-states-5 flex-b-100--mob flex-b-49--mob-u flex-b-32--tb-u br-sm p-1 bg-white mb-md zoomindown__animate">
                                <i class="iconify icon mb-md box-content br-50 p-sm br-50 b-shadow-primary c-orange <?php echo esc_attr( $service->service_item_icon ); ?>"></i>
                                <h3 class="fw-600 underline underline-center ps-relative f-mulish h3 mb-md"><?php echo esc_html( $service->service_item_title ); ?></h3>
                                <p class="c-offblack ln-1"><?php echo esc_html( $service->service_item_desc ); ?></p>
                            </div>
                        <?php } ?>

                </div>
            </div>
        </section>
    <?php }

}

add_action( 'olympus_action_services', 'olympus_action_services', 10 );

if( !function_exists( 'olympus_action_about' ) ) {

    function olympus_action_about() { 
            if( 'enable' != get_theme_mod( 'olympus_about_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="p-sm container mx-auto pb-5">
                <div class="d-flex align-center flex-wrap--tb">
                    <div class="d-flex align-center mb-md--lp mr-3--tb-u fadein__animate">
                        <img class="w-100" src="<?php echo esc_url( get_theme_mod( 'about_us_featured_image' ) ); ?>" alt="a mature lawyer">
                    </div>
                    <div class="fadeinright__animate">
                        <h2 class="h2 f-mulish fw-800 mb-md"><?php echo esc_html( get_theme_mod( 'olympus_about_us_title' ) ); ?></h2>
                        <p class="ln-1 mb-md c-offblack"><?php echo esc_html( get_theme_mod( 'olympus_about_us_desc' ) ); ?></p>

                        <?php 
                            $about_us_features = json_decode( get_theme_mod( 'olympus_about_us_features' ) ); 
                            foreach( $about_us_features as $about_us_feature ) { ?>
                                <div class="mb-md d-flex align-center b-shadow-primary content-box px-sm py-xm">
                                    <i class="iconify mr-1 icon-1 c-offblack <?php echo esc_attr( $about_us_feature->feature_icon ); ?>"></i>
                                    <div>
                                        <h3 class="h3 c-orange f-mulish mb-sm"><?php echo esc_html( $about_us_feature->feature_title ); ?></h3>
                                        <p class="c-offblack ln-1"><?php echo esc_html( $about_us_feature->feature_desc ); ?></p>
                                    </div>
                                </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_about', 'olympus_action_about', 10 );

if( !function_exists( 'olympus_action_counter' ) ) {

    function olympus_action_counter() { 
            if( 'enable' != get_theme_mod( 'olympus_counter_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="experience bg-default">

                <div class="bg-primary-1">
                    <div class="container mx-auto text-center c-white p-sm py-7">
                        <div class="fade__animate">
                            <h2 class="h2 f-mulish mb-lg underline underline-center ps-relative"><?php echo esc_html( get_theme_mod( 'olympus_counter_title' ) ); ?></h2>
                            <p class="ln-1 mb-lg fw-100 max-w-600 mx-auto f-mulish">
                                <?php echo esc_html( get_theme_mod( 'olympus_counter_desc' ) ); ?>
                            </p>
                        </div>

                        <div class="d-flex justify-evenly flex-wrap fadein__animate">

                            <?php 
                                $counter_items = json_decode( get_theme_mod( 'olympus_counter_items' ) );
                                foreach( $counter_items as $counter_item ) { ?>
                                    <div class="p-sm px-sm bg-primary mb-sm mx-1 w-100--mob d-flex align-center b-shadow-primary border-primary">
                                        <i class="iconify icon mr-1 c-orange <?php echo esc_attr( $counter_item->counter_icon ); ?>"></i>
                                        <div>
                                            <h4 class="h1 f-mulish"><?php echo esc_html( $counter_item->counter_no ); echo esc_html( $counter_item->counter_no_suffix ); ?></h4>
                                            <p class="ln-1 fw-100"><?php echo esc_html( $counter_item->counter_title ); ?></p>
                                        </div>
                                    </div>
                            <?php } ?>

                        </div>

                    </div>
                    <h2 class="h2 bg-white f-mulish py-md legal-practic text-center"><?php echo esc_html( get_theme_mod( 'olympus_legal_practice_title' ) ); ?></h2>
                </div>

            </div>

        </section>

    <?php }

}

add_action( 'olympus_action_counter', 'olympus_action_counter', 10 );

if( !function_exists( 'olympus_action_legal_practice' ) ) {

    function olympus_action_legal_practice() { 
            if( 'enable' != get_theme_mod( 'olympus_legal_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="container mx-auto p-sm py-lg">

                <div class="d-flex justify-between flex-wrap mb-md">

                    <?php 
                        $legal_practice_items = json_decode( get_theme_mod( 'olympus_legal_practice_items' ) ); 
                        foreach( $legal_practice_items as $legal_practice_item ) { ?>
                            <div class="bg-primary c-white p-sm br-sm b-shadow-primary d-flex flex-b-32--tb-u flex-b-49--tb flex-b-100--mob mb-md zoomindown__animate">
                                <div class="mr-1">
                                    <i class="iconify icon c-orange <?php echo esc_attr( $legal_practice_item->legal_practice_icon ); ?>"></i>
                                </div>

                                <div>
                                    <h5 class="fs-md ln-1 f-mulish ps-relative underline mb-md"><?php echo esc_html( get_the_title( $legal_practice_item->legal_practice_item_page ) ); ?></h5>
                                    <p class="ln-1 c-offwhite">
                                    <?php 
                                        echo wp_strip_all_tags( apply_filters( 'the_excerpt', get_post_field( 'post_excerpt', $legal_practice_item->legal_practice_item_page ) ) );
                                    ?>
                                    </p>
                                </div>
                            </div>
                    <?php } ?>

                </div>

                <div class="text-center">
                    <a class="bg-secondary btn-states cr-pointer btn btn-sm bounce__animate" href="<?php echo esc_url( get_theme_mod( 'olympus_legal_practice_button_link' ) ); ?>"><?php echo esc_html( get_theme_mod( 'olympus_legal_practice_button_text' ) ); ?></a>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_legal_practice', 'olympus_action_legal_practice', 10 );

if( !function_exists( 'olympus_action_notice' ) ) {

    function olympus_action_notice() { 
            if( 'enable' != get_theme_mod( 'olympus_notice_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="container mx-auto p-sm py-lg">
                <div class="d-flex align-center flex-wrap--mob flex-wrap--tb">

                    <div class="max-w-300--tb-u mr-3--tb-u mb-lg--tb flex-shrink-0--tb-u fadeinleft__animate">
                        <p class="c-offblack mb-sm"><?php echo esc_html( get_theme_mod( 'olympus_notice_title' ) ); ?></p>
                        <h2 class="h2 f-mulish fw-800"><?php echo esc_html( get_theme_mod( 'olympus_notice_desc' ) ); ?></h2>
                    </div>

                    <div class="fadeinright__animate">
                        <h3 class="h3 mb-sm"><?php echo esc_html( get_the_title( get_theme_mod( 'olympus_notice_page' ) ) ); ?></h3>
                        <div class="c-offblack mb-sm ln-1">
                            <?php
                                $post   = get_post( get_theme_mod( 'olympus_notice_page' ) );
                                echo apply_filters( 'the_content', $post->post_content );
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_notice', 'olympus_action_notice', 10 );

if( !function_exists( 'olympus_action_contact' ) ) {

    function olympus_action_contact() { 
            if( 'enable' != get_theme_mod( 'olympus_contact_enable_disable', 'enable' ) ) return;
        ?>

        <section class="bg-offwhite circle ps-relative overflow-hidden">
            <div class="container mx-auto p-sm py-lg overflow-hidden ps-relative">
                <div class="d-flex align-center flex-wrap--lp">
                    <div class="mr-3--mob-u frame-orange pt-md pl-md flex-b-100--lp">
                        <?php echo do_shortcode( get_theme_mod( 'olympus_contact_short_code' ) ); ?>
                    </div>

                    <div class="fadeinright__animate">
                        <h2 class="h2 fw-800 h2-off--lp-u bg-white p-sm pl-md ps-relative f-mulish mb-md mt-6"><?php echo esc_html( get_the_title( get_theme_mod( 'olympus_contact_page' ) ) ); ?></h2>
                        <div class="c-offblack ln-1 mb-sm">
                        <?php
                            $post   = get_post( get_theme_mod( 'olympus_contact_page' ) );
                            echo apply_filters( 'the_content', $post->post_content );
                        ?>
                        </div>
                        <h3 class="h3 mb-sm"><?php echo esc_html( get_theme_mod( 'olympus_contact_name' ) ); ?></h3>
                        <p class="c-offblack ln-1 mb-sm"><?php echo esc_html( get_theme_mod( 'olympus_contact_designation' ) ); ?></p>
                        <p class="f-tertiary h3 signature"><?php echo esc_html( get_theme_mod( 'olympus_contact_signature' ) ); ?></p>
                    </div>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_contact', 'olympus_action_contact', 10 );

if( !function_exists( 'olympus_action_tab' ) ) {

    function olympus_action_tab() { 
            if( 'enable' != get_theme_mod( 'olympus_tab_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="recent-work">
                <div class="bg-primary-1">
                    <section>
                        <div class="container mx-auto p-sm py-5">
                            <div class="fade__animate">
                                <h2 class="h2 f-mulish mb-lg ps-relative c-white underline"><?php echo esc_html( get_theme_mod( 'olympus_tab_title' ) ); ?></h2>
                                <p class="ln-1 c-offwhite mb-lg max-w-700">
                                    <?php echo esc_html( get_theme_mod( 'olympus_tab_desc' ) ); ?>
                                </p>
                            </div>

                            <div class="d-flex flex-wrap--lp align-center ps-relative ">
                                <div
                                    class="tx-10--lp-u d-flex--lp flex-wrap justify-evenly mx-auto--lp flex-grow--lp fadeinleft__animate">
                                    <?php 
                                        $tab_items = json_decode( get_theme_mod( 'olympus_tab_items' ) ); 
                                        foreach( $tab_items as $tab_item ) { ?>
                                            <button id="rw1-personal-injury"
                                                class="d-flex p-1 btn-states w-100 w-17r--mob-u w-20r--lp-u pl-md stick ps-relative fs-md-1 mb-md align-center cr-pointer bg-primary br-none c-white">
                                                <i class="iconify mr-1 <?php echo esc_attr( $tab_item->tab_item_icon ); ?>"></i>
                                                <?php echo esc_html( get_the_title( $tab_item->tab_item_page ) ); ?>
                                            </button>
                                        <?php }
                                    ?>
                                </div>

                                <div
                                    class="c-offblack ln-1 mx-auto--lp p-sm d-flex flex-wrap justify-between bg-white b-shadow-primary p-4--lp-u mb-md--lp fadeinright__animate">
                                    <?php
                                        foreach( $tab_items as $tab_item ) {
                                            $post   = get_post( $tab_item->tab_item_page );
                                            echo apply_filters( 'the_content', $post->post_content );
                                        }
                                    ?>
                                </div>

                                <div
                                    class="f-mulish d-none--lp bg-secondary c-white p-sm fw-800 ps-absolute r-0 t-0 ty-n-20">
                                    <h1 class="h1"><?php echo esc_html( get_theme_mod( 'olympus_tab_number' ) ); ?> <sup><?php echo esc_html( get_theme_mod( 'olympus_tab_number_suffix' ) ); ?></sup> </h1>
                                    <p class="text-right"><?php echo esc_html( get_theme_mod( 'olympus_tab_number_title' ) ); ?></p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_tab', 'olympus_action_tab', 10 );

if( !function_exists( 'olympus_action_testimonials' ) ) {

    function olympus_action_testimonials() { 
            if( 'enable' != get_theme_mod( 'olympus_testimonials_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="container mx-auto p-sm py-5 text-center">
                <div class="fade__animate">
                    <h2 class="h2 f-mulish mb-lg ps-relative underline underline-center"><?php echo esc_html( get_theme_mod( 'olympus_testimonials_title' ) ); ?></h2>
                    <p class="ln-1 c-offblack f-mulish max-w-600 mx-auto mb-lg">
                        <?php echo esc_html( get_theme_mod( 'olympus_testimonials_desc' ) ); ?>
                    </p>
                </div>

                <div class="px-lg--tb-u box-content max-w-700 mx-auto">
                    <div class="glide fadein__animate">
                        <div class="glide__track" data-glide-el="track">
                            <ul class="glide__slides">
                            <?php 
                                $testimonials_items = json_decode( get_theme_mod( 'olympus_testimonials_items' ) ); 
                                foreach( $testimonials_items as $testimonial_item ) { ?>
                                    <li class="glide__slide ps-relative">
                                        <div
                                            class="bg-primary-2 bg-primary-2__shadow p-2 py-lg box-content br-sm c-white ps-relative">
                                            <div class="bg-primary p-2">
                                                <h3 class="h3 mb-lg f-mulish underline underline-center ps-relative"><?php echo esc_html( get_the_title( $testimonial_item->testimonial_item_page ) ); ?></h3>
                                                <div class="ln-1 c-offwhite mb-sm">
                                                    <?php 
                                                        $post   = get_post( $testimonial_item->testimonial_item_page );
                                                        echo apply_filters( 'the_content', $post->post_content );
                                                    ?>
                                                </div>
                                                <p class="ln-1"><?php echo esc_html( $testimonial_item->testimonial_item_designation ); ?></p>
                                                <span class="iconify icon-5 ps-absolute t-0 r-0 c-offwhite"
                                                    data-icon="ci:single-quotes-r"></span>
                                                <span class="iconify icon-5 ps-absolute b-0 l-0 c-offwhite"
                                                    data-icon="ci:single-quotes-l"></span>
                                            </div>
                                        </div>
                                    </li>
                                <?php }
                            ?>
                            </ul>
                        </div>

                        <div class="glide__arrows d-none--tb" data-glide-el="controls">
                            <button
                                class="glide__arrow box-content px-md swipe-arrow swipe-arrow__left br-none b-shadow-none glide__arrow--left"
                                data-glide-dir="<">
                                <span class="iconify h2" data-icon="la:long-arrow-alt-left"></span>
                            </button>
                            <button
                                class="glide__arrow box-content px-md swipe-arrow swipe-arrow__right br-none b-shadow-none glide__arrow--right"
                                data-glide-dir="<">
                                <span class="iconify h2" data-icon="la:long-arrow-alt-right"></span>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_testimonials', 'olympus_action_testimonials', 10 );

if ( !function_exists( 'olympus_action_team' ) ) {

    function olympus_action_team() { 
            if( 'enable' != get_theme_mod( 'olympus_team_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="bg-offwhite">
                <div class="container mx-auto py-lg p-sm text-center">
                    <div class="fade__animate">
                        <h2 class="h2 mb-lg ps-relative underline underline-center"><?php echo esc_html( get_theme_mod( 'olympus_team_title' ) ); ?></h2>
                        <p class="ln-1 c-offblack max-w-600 mx-auto mb-lg f-mulish">
                            <?php echo esc_html( get_theme_mod( 'olympus_team_desc' ) ); ?>
                        </p>
                    </div>

                    <div class="d-flex justify-between flex-wrap">
                        <?php 
                            $team_items = json_decode( get_theme_mod( 'olympus_team_items' ) );
                            foreach( $team_items as $team_item ) { ?>
                                <div
                                    class="flex-b-32--tb-u flex-b-49--tb flex-b-100--mob mb-md c-white b-shadow-primary zoomindown__animate">
                                    <div class="d-flex align-center">
                                        <img class="w-100" src="<?php echo esc_url( get_the_post_thumbnail_url( $team_item->item_page ) ); ?>" alt="">
                                    </div>
                                    <p class="ln-1 bg-primary-2 p-xm"><?php echo esc_html( $team_item->item_designation ); ?></p>
                                    <div class="bg-primary p-sm">
                                        <div class="ln-1 mb-sm c-offwhite">
                                            <?php
                                                $post   = get_post( $team_item->item_page );
                                                echo apply_filters( 'the_content', $post->post_content );
                                            ?>
                                        </div>
                                        <p class="ln-1 mb-sm f-mulish fw-800 fs-md underline-center underline ps-relative"><?php echo esc_html( get_the_title( $team_item->item_page ) ); ?>
                                        </p>

                                    </div>
                                </div>
                            <?php }
                        ?>

                    </div>

                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_team', 'olympus_action_team', 10 );

if ( !function_exists( 'olympus_action_logo' ) ) {

    function olympus_action_logo() { 
            if( 'enable' != get_theme_mod( 'olympus_logo_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="container p-sm mx-auto py-lg ">
                <div class="fade__animate text-center">
                    <h2 class="h2 mb-lg ps-relative underline underline-center"><?php echo esc_html( get_theme_mod( 'olympus_logo_title' ) ); ?></h2>
                    <p class="ln-1 c-offblack max-w-600 mx-auto mb-lg f-mulish">
                        <?php echo esc_html( get_theme_mod( 'olympus_logo_desc' ) ); ?>
                    </p>
                </div>
                <div class="d-flex flex-wrap justify-between fadein__animate">
                    <?php 
                        $logo_items = json_decode( get_theme_mod( 'olympus_logo_items' ) ); 
                        foreach( $logo_items as $logo_item ) { ?>
                            <img class="max-w-11" src="<?php echo esc_url( $logo_item->logo ); ?>" alt="">
                        <?php }
                    ?>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_logo', 'olympus_action_logo', 10 );

if ( !function_exists( 'olympus_action_blog' ) ) {

    function olympus_action_blog() { 
            if( 'enable' != get_theme_mod( 'olympus_blog_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="bg-offwhite">
                <div class="container p-sm mx-auto py-lg">
                    <h2 class="h2 mb-lg f-mulish text-center ps-relative underline underline-center fade__animate"><?php echo esc_html( get_theme_mod( 'olympus_blog_title' ) ); ?></h2>
                    <p class="ln-1 c-offblack max-w-600 mx-auto mb-lg f-mulish text-center">
                        <?php echo esc_html( get_theme_mod( 'olympus_blog_desc' ) ); ?>
                    </p>

                    <?php

                        $blog           = get_theme_mod( 'olympus_blog_cat' );
                        $cat_id         = explode( ',', $blog );
                        $blog_posts_no  = get_theme_mod( 'olympus_blog_posts_no', '3' );

                        $args = array(
                            'posts_per_page' => 1,
                            'post_type' => 'post',
                            'order' => 'desc',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'term_id',
                                    'terms' => $cat_id
                                ),
                            ),
                        );

                        $blog_query = new WP_Query ( $args );

                        if ( $blog_query->have_posts() ): while ( $blog_query->have_posts() ) : $blog_query->the_post();

                        ?>

                            <img class="max-w-30--tb-u w-100 mr-3 float-left mb-sm fade__animate" src="../img/blog-image@2x.jpg" alt="">
                            <div class="fadeinright__animate">
                                <div class="d-inline-flex--tb d-flex--tb-u mb-md align-center">
                                    <span class="iconify flex-shrink-0 fs-sm c-offblack mr-1" data-icon="akar-icons:clock"></span>
                                    <p class="c-offblack fs-sm mr-1 fw-500"><?php echo esc_html( get_the_date() ); ?></p>
                                    <span class="iconify flex-shrink-0 fs-sm c-offblack mr-1" data-icon="et:chat"></span>
                                    <p class="c-offblack fs-sm fw-500">Author: <?php the_author(); ?></p>
                                </div>
                                <h3 class="fw-800 h3 ps-relative underline fs-md f-mulish mb-lg">
                                <?php the_title(); ?>
                                </h3>
                                <div class="ln-1 c-offblack mb-md">
                                    <?php the_content(); ?>
                                </div>

                                <div class="d-flex align-center">
                                    <div class="badge br-50 mr-1">
                                        <?php 
                                            $get_author_id          = get_the_author_meta( 'ID' );
                                            $get_author_gravatar    = get_avatar_url( $get_author_id, array( 'size' => 80 ) );
                                        ?>
                                        <img class="grid-img" src="<?php echo esc_url( $get_author_gravatar ); ?>" alt="">
                                    </div>
                                    <div>
                                        <p class="fw-800 ln-1"><?php the_author(); ?></p>
                                        <p class="ln-1 fs-sm c-offblack">
                                            <?php echo esc_html( wp_trim_words( get_the_author_meta( 'user_description' ), 5 ) ); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile; endif;

                        ?>
                </div>
            </div>
        </section>

        <section>
            <div>
                <div class="container p-sm py-lg mx-auto">
                    <div class="d-flex justify-between flex-wrap">
                    <?php
                        $blog           = get_theme_mod( 'olympus_blog_cat' );
                        $cat_id         = explode( ',', $blog );
                        $blog_posts_no  = get_theme_mod( 'olympus_blog_posts_no', '3' );

                        $args = array(
                            'posts_per_page' => $blog_posts_no,
                            'post_type' => 'post',
                            'order' => 'desc',
                            'offset' => '1',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'term_id',
                                    'terms' => $cat_id
                                ),
                            ),
                        );

                        $blog_query = new WP_Query ( $args );

                        if ( $blog_query->have_posts() ): while ( $blog_query->have_posts() ) : $blog_query->the_post();

                        ?>
                            <div
                                class="flex-b-32--tb-u flex-b-49--tb flex-b-100--mob mb-md c-white b-shadow-primary text-center zoomindown__animate">
                                <div class="d-flex align-center">
                                    <img class="w-100" src="<?php the_post_thumbnail_url(); ?>" alt="">
                                </div>

                                <div class="p-sm">
                                    <div class="d-flex align-center mb-sm">
                                        <span class="iconify flex-shrink-0 c-offblack fs-sm mr-1"
                                            data-icon="akar-icons:clock"></span>
                                        <p class="c-offblack fs-sm mr-1 fw-500"><?php echo esc_html( get_the_date() ); ?></p>
                                        <span class="iconify flex-shrink-0 c-offblack fs-sm mr-1" data-icon="et:chat"></span>
                                        <p class="c-offblack fs-sm fw-500">Author: <?php the_author(); ?></p>
                                    </div>
                                    <p class="fs-md f-mulish c-black fw-800 mb-sm"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                                    <div class="ln-1 c-offblack mb-sm">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="input-fs btn-states cr-pointer btn btn-form p-xm br-none bg-secondary c-white">
                                        <?php echo esc_html( get_theme_mod( 'olympus_blog_button_text' ) ); ?>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_blog', 'olympus_action_blog', 10 );

if ( !function_exists( 'olympus_action_cta' ) ) {

    function olympus_action_cta() { 
            if( 'enable' != get_theme_mod( 'olympus_cta_enable_disable', 'enable' ) ) return;
        ?>

        <section>
            <div class="container mx-auto p-sm fadein__animate mb-md">
                <div class="d-flex flex-wrap align-center box-content p-1 py-md c-white align-center justify-center bg-primary br-sm">
                    <div class="mb-md d-flex">
                        <p class="fw-800 fs-md-1 f-mulish mr-3 ps-relative stick-subscribe mb-sm--lp"><?php echo esc_html( get_theme_mod( 'olympus_cta_title' ) ); ?></p>
                        <p class="ln-1 flex-grow mr-1 c-offwhite mb-sm--lp text-center--lp"><?php echo esc_html( get_theme_mod( 'olympus_cta_desc' ) ); ?></p>
                    </div>

                    <div class="flex-b-100 flex-shrink-0 d-flex flex-wrap justify-center">
                        <?php echo do_shortcode( get_theme_mod( 'olympus_cta_short_code' ) ); ?>
                    </div>
                </div>
            </div>
        </section>

    <?php }

}

add_action( 'olympus_action_cta', 'olympus_action_cta', 10 );

function olympus_numbered_pagination() {

    global $wp_query;
    $big = 9999999; // need an unlikely integer
      echo paginate_links( array(
       'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
       'format' => '?paged=%#%',
       'current' => max( 1, get_query_var('paged') ),
       'total' => $wp_query->max_num_pages) );
}

?>