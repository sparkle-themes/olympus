<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Olympus
 */

get_header();
?>

<section>
	<div class="hero bg-default">
		<div class="bg-primary-1">
			<div class="container c-white mx-auto py-7 p-sm text-center">
				<h1 class="h2 f-mulish mb-md"><?php the_title(); ?></h1>
				<p class="f-mulish fs-md-1">Home > <span class="c-orange"><?php the_title(); ?></span> </p>
			</div>
		</div>
	</div>
</section>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>

	
<?php
get_footer();