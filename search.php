<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Olympus
 */

global $wp_query;
get_header();

?>

<section>
	<div class="hero bg-default">
		<div class="bg-primary-1">
			<div class="container mx-auto py-7 p-sm text-center">
				<h1 class="h2 c-white f-mulish mb-md"><?php echo esc_html__( 'Search Result', 'olympus' ); ?></h1>
				<p class="f-mulish fs-md-1 c-orange"><?php echo esc_html__( get_search_query() ); ?></p>
			</div>
		</div>
	</div>
</section>
	

		<?php if ( have_posts() ) : ?>

			<section>
				<div class="container mx-auto p-sm py-lg">
					<h1 class="fs-md-1 p-sm c-offblack f-mulish mb-md ps-relative underline search-underline"><?php echo esc_html( $wp_query->found_posts ) ?> Result found
						for <?php echo esc_html__( "'" . get_search_query() . "'" ); ?>	</h1>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			olympus_numbered_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
	</section>

<?php
get_footer();