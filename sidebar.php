<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Olympus
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="p-md box-content d-none--tb flex-grow-0 flex-shrink-0 flex-b-22">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->