<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Olympus
 */

get_header();
?>

	<section>
		<div class="hero bg-default">
			<div class="bg-primary-1">
				<div class="container c-white mx-auto py-7 p-sm text-center">
					<h1 class="h2 f-mulish mb-md"><?php the_title(); ?></h1>
					<p class="f-mulish fs-md-1">Home > <span class="c-orange"><?php the_title(); ?></span> </p>
				</div>
			</div>
		</div>
	</section>

	<?php
		while ( have_posts() ) :
			the_post();

			?>

			<section>
				<div class="container mx-auto p-sm py-lg">
					<div class="d-flex">
						<div class="flex-grow flex-shrink">

			<?php

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'olympus' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'olympus' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.

		?>
			</div>
		<?php

		get_sidebar();
	?>

		</div>
	</div>
</section>

<?php
get_footer();