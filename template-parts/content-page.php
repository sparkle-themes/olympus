<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Olympus
 */

?>

<section>
	<div class="container mx-auto p-sm py-lg">
		<div class="d-flex">

			<div class="flex-grow flex-shrink">
				<img src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>" class="w-100 mb-md fadeinleft__animate" alt="">

				<div class="fade__animate">
					<div class="d-inline-flex align-center mb-md br-sm justify-evenly c-offblack bg-offwhite p-xm">
						<span class="iconify flex-shrink-0 fs-sm mr-1" data-icon="akar-icons:clock"></span>
						<p class="fs-sm mr-1 fw-500"><?php echo esc_html( get_the_date() ); ?></p>
						<span class="iconify flex-shrink-0  fs-sm mr-1" data-icon="et:chat"></span>
						<p class="fs-sm fw-500 mr-1"><?php echo esc_html__( 'Comments: ', 'olympus' ); echo esc_html( get_comments_number() ); ?></p>
						<span class="iconify flex-shrink-0 fs-sm mr-1" data-icon="et:chat"></span>
						<p class="fs-sm fw-500"><?php echo esc_html__( 'Author: ', 'olympus' ); ?><span class="fw-800"><?php echo esc_html( get_the_author() ); ?></span> </p>
					</div>

					<h3 class="h3 fw-800 ps-relative underline mb-lg f-mulish"><?php echo esc_html( get_the_title() ); ?></h3>
				</div>

				<div class="fade__animate">

					<div class="c-offblack ln-1 f-mulish mb-sm ">
						<?php the_content(); ?>
					</div>

				</div>

				<div class="fade__animate">

					<div class="p-sm bg-offwhite mb-md">
						<div class="d-flex align-center mb-sm">
							<div class="badge br-50 mr-1">
								<?php 
									$get_author_id          = get_the_author_meta( 'ID' );
									$get_author_gravatar    = get_avatar_url( $get_author_id, array( 'size' => 80 ) );
								?>
								<img class="grid-img" src="<?php echo esc_url( $get_author_gravatar ); ?>" alt="">
							</div>

							<div>
								<h4 class="fw-800 ln-1 fs-md-1"><?php echo esc_html( ucfirst( get_the_author() ) ); ?></h4>
								<p class="ln-1 fs-sm c-offblack">
									<?php echo esc_html( get_the_author_meta( 'user_description' ) ); ?>
								</p>
							</div>
						</div>

					</div>
				</div>

			</div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</section>