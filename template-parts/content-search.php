<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Olympus
 */

?>


<div class="bg-offwhite d-flex mb-lg flex-wrap--mob align-center p-sm backinleft__animate">

	<div class="search-img--mob-u w-100--mob flex-shrink-0 mr-2--mob-u mb-sm--mob">
		<img class="w-100" src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>" alt="">
	</div>

	<div class="flex-grow">
		<h2 class="fs-md mb-xm f-mulish"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<div class="ln-1 c-offblack mb-xm">
		<?php 
			the_excerpt();
		?>
		</div>
	</div>
</div>